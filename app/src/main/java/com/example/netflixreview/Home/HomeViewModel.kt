package com.example.netflixreview.Home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel: ViewModel() {
    private val _title = MutableLiveData<List<Data>>()
    val title: LiveData<List<Data>>
        get() = _title


    private fun setData(){
        _title.value = listOf(
            Data("Vagabond","Two of the Korean entertainment industry’s biggest names—Bae Suzy (of girl group Miss A, better known mononymously as “Suzy”) and Lee Seung-gi (of Brilliant Legacy, The King 2 Hearts, and more)—are back in the spotlight with a new conspiracy drama. With episodes hitting Netflix worldwide every Friday and Saturday for eight weeks starting Sep 20, 2019, Vagabond is the latest big budget K-drama with international ambitions. The series certainly feels like a blockbuster, in ways both positive and negative. Vagabond offers good entertainment value while showcasing South Korea’s status as a middle power, but doesn’t necessarily push the envelope beyond standard K-drama tropes."),
            Data("Avenger Endgame","Avengers: Endgame picks up after the events of Avengers: Infinity War, which saw the Avengers divided and defeated. Thanos won the day and used the Infinity Stones to snap away half of all life in the universe. Only the original Avengers - Iron Man, Captain America, Thor, Hulk, Black Widow, and Hawkeye remain, along with some key allies like War Machine, Ant-Man, Rocket Raccoon, Nebula, and Captain Marvel. Each of the survivors deals with the fallout from Thanos' decimation in different ways, but when an opportunity presents itself to potentially save those who vanished, they all come together and set out to defeat Thanos, once and for all."),
            Data("Thor","The warrior Thor (Chris Hemsworth) is cast out of the fantastic realm of Asgard by his father Odin (Sir Anthony Hopkins) for his arrogance and sent to Earth to live amongst humans. Falling in love with scientist Jane Foster (Natalie Portman) teaches Thor much-needed lessons, and his new-found strength comes into play as a villain from his homeland sends dark forces toward Earth."),

            Data("Fast 7","Dominic and his crew thought they'd left the criminal mercenary life behind. They'd defeated international terrorist Owen Shaw and went their separate ways. But now, Shaw's brother, Deckard Shaw, is out killing the crew one by one for revenge. Worse, a Somalian terrorist called Jakarde and a shady government official called \"Mr. Nobody\" are both competing to steal a computer terrorism program called \"God's Eye,\" that can turn any technological device into a weapon. Torretto must reconvene with his team to stop Shaw and retrieve the God's Eye program while caught in a power struggle between the terrorist and the United States government."),
            Data("Begin Again", "Gretta (Keira Knightley) and her long-time boyfriend Dave (Adam Levine) are college sweethearts and songwriting partners who decamp for New York when he lands a deal with a major label. But the trappings of his new-found fame soon tempt Dave to stray, and a reeling, lovelorn Gretta is left on her own. Her world takes a turn for the better when Dan (Mark Ruffalo), a disgraced record-label exec, stumbles upon her performing on an East Village stage and is immediately captivated by her raw talent. From this chance encounter emerges an enchanting portrait of a mutually transformative collaboration, set to the soundtrack of a summer in New York City.")
        )
    }

    init {
        setData()
    }

    override fun onCleared() {
        super.onCleared()
    }
}