package com.example.netflixreview.Home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.netflixreview.R
import com.google.android.material.snackbar.Snackbar

class Adapter : RecyclerView.Adapter<Adapter.ViewHolder>() {
    var data = listOf<Data>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adapter.ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: Adapter.ViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item)
    }

    class ViewHolder private constructor(itemView: View) :RecyclerView.ViewHolder(itemView){
        //ผูก ViewHolder กับ layout
        val titleTextView: TextView = itemView.findViewById(R.id.titleTextView)
        val contentTextView: TextView = itemView.findViewById(R.id.contentTextView)
        val detailBtn: Button = itemView.findViewById(R.id.detailBtn)
        val image: ImageView = itemView.findViewById(R.id.detailImage)

        fun bind(item: Data) {
            titleTextView.text = item.title
            contentTextView.text = item.content
            when (item.title) {
                "Vagabond" -> image.setImageResource(R.drawable.vagabond)
                "Avenger Endgame" -> image.setImageResource(R.drawable.avenger)
                "Thor" -> image.setImageResource(R.drawable.thor)
                "Fast 7" -> image.setImageResource(R.drawable.fast7)
                "Begin Again" -> image.setImageResource(R.drawable.begin)
            }

            detailBtn.setOnClickListener {
                it.findNavController().navigate(homeFragmentDirections.actionHomeFragmentToDetailFragment(item.title,item.content))

            }
        }
        companion object{
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.text_item_view, parent, false)

                return ViewHolder(view)
            }
        }

    }

}