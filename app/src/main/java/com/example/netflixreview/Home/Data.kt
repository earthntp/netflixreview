package com.example.netflixreview.Home

import android.widget.ImageView

data class Data (
    val title: String,
    val content: String
)