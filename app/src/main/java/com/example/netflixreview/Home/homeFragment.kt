package com.example.netflixreview.Home


import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.netflixreview.R
import com.example.netflixreview.databinding.FragmentHomeBinding
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.text_item_view.*
import timber.log.Timber
import android.view.MenuItem as MenuItem

/**
 * A simple [Fragment] subclass.
 */
class homeFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentHomeBinding>(inflater, R.layout.fragment_home,container,false)
        val viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        setHasOptionsMenu(true)
        val adapter = Adapter()
        binding.movieList.adapter = adapter
        viewModel.title.observe(this, Observer {
            it?.let {
                adapter.data = it
            }
        })
        Timber.i("HomeFragment Called")
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var snack = view?.let { Snackbar.make(it, "Hello Home Fragment", Snackbar.LENGTH_LONG) }
        snack?.show()

    }

}
